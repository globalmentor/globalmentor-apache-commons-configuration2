# GlobalMentor Apache Commons Configuration 2

GlobalMentor additions to the [Apache Commons Configuration 2.x](https://commons.apache.org/proper/commons-configuration/) library.

## Download

GlobalMentor Apache Commons Configuration 2 is available in the Maven Central Repository as [com.globalmentor:globalmentor-apache-commons-configuration2](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22com.globalmentor%22%20AND%20a%3A%22globalmentor-apache-commons-configuration2%22).

## Issues

Issues tracked by [JIRA](https://globalmentor.atlassian.net/projects/JAVA).

## Changelog

- 1.1.0:
	* [JAVA-35](https://globalmentor.atlassian.net/browse/JAVA-35): Improved dependencies on the POM for the current api.
	* [JAVA-24](https://globalmentor.atlassian.net/browse/JAVA-24): Added ways to shutdown the watch service by using jvm features.
- 1.0.0: (2017-02-06) First public release.
	* [JAVA-18](https://globalmentor.atlassian.net/browse/JAVA-18): Create Apache Commons Configuration watch service reloading trigger.
