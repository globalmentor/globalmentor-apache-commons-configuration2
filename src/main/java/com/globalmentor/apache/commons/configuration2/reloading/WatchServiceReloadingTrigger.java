/*
 * Copyright © 2017 GlobalMentor, Inc. <http://www.globalmentor.com/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.globalmentor.apache.commons.configuration2.reloading;

import java.io.*;
import java.lang.ref.*;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;

import javax.annotation.*;

import org.apache.commons.configuration2.reloading.*;

import static com.globalmentor.java.Conditions.*;

/**
 * A trigger to make reloading checks based upon a {@link WatchService}.
 * 
 * <p>
 * It stores a {@link WeakReference} to the {@link ReloadingController} that must be provided on the constructor. If all its strong references are lost this
 * watch service will shutdown at the moment that the {@link WatchService} provides a {@link WatchKey} or if its check for the key expire, whatever happens
 * first. The timeout for the {@link WatchService} is set up to happens once a day.
 * 
 * This approach was chosen in order to shutdown the {@link WatchService} for situations that the application forget to close this instance when the
 * {@link ReloadingController} isn't being used anymore.
 * </p>
 * 
 * @author Magno Nascimento
 */
public class WatchServiceReloadingTrigger implements AutoCloseable {

	/**
	 * The trigger task that will handle with the calls to {@link ReloadingController#checkForReloading(Object)} when an event occurs with the watched
	 * {@link Path}.
	 */
	private WatchServiceTriggerTask triggerTask;

	/** The watcher used to verify whether a modification on the file has occurred. */
	private final WatchService watcher = FileSystems.getDefault().newWatchService();

	/** The {@link Path} for the configuration file to be watched. */
	private final Path configurationFilePath;

	/** The reference queue to handle with the weak reference to {@link ReloadingController}. */
	private final ReferenceQueue<ReloadingController> referenceQueue = new ReferenceQueue<ReloadingController>();

	/** The {@link ReloadingController} of the configuration which will be notified whenever the configuration file is modified. */
	private final WeakReference<ReloadingController> controller;

	/** The parameter to be passed to the {@link ReloadingController} when it checks for reloading. */
	private final Object controllerParam;

	/**
	 * Creates a new instance of {@link WatchServiceReloadingTrigger}.
	 * 
	 * @param ctrl The {@link ReloadingController} of the configuration which will be warned whenever the configuration file is modified.
	 * @param ctrlParam The parameter to be passed to the {@link ReloadingController} when it checks for reloading, if any.
	 * @param configurationFilePath The {@link Path} for the configuration file to be watched.
	 * @throws IllegalArgumentException if the given {@link Path} does not refer to a file.
	 * @throws IOException if an I/O error occurs.
	 */
	public WatchServiceReloadingTrigger(@Nonnull final ReloadingController ctrl, @Nullable final Object ctrlParam, @Nonnull final Path configurationFilePath)
			throws IOException {
		checkArgumentNotNull(ctrl, "Reloading controller must not be null");
		checkArgumentNotNull(configurationFilePath, "The path for the configuration file must not be null");
		checkArgument(!Files.isDirectory(configurationFilePath), "The given path must refer to a file");

		this.configurationFilePath = configurationFilePath.toAbsolutePath(); //the Path is converted to an absolute Path because the getParent() method does not access the file system, which means that it might return null in case of the parent not be listed on its relative Path
		this.configurationFilePath.getParent().register(watcher, StandardWatchEventKinds.ENTRY_MODIFY);

		this.controller = new WeakReference<ReloadingController>(ctrl, referenceQueue);

		this.controllerParam = ctrlParam;
	}

	/** Starts this trigger. If this trigger is already started, this invocation has no effect. */
	public synchronized void start() {

		if(!isRunning()) {
			triggerTask = new WatchServiceTriggerTask();
			triggerTask.setDaemon(true);
			triggerTask.start();
		}

	}

	/**
	 * Stops this trigger. The associated {@code ReloadingController} is no more triggered. If this trigger is already stopped, this invocation has no effect.
	 * <p>
	 * If this watcher is asked to stop while in a step of verification for events, it will be stopped only after it finishes its current verification.
	 * </p>
	 */
	public synchronized void stop() {

		if(isRunning()) {
			triggerTask.turnOff();
		}

	}

	/**
	 * This implementation just delegates to {@link #stop()}. It was created in order to make the class implements {@link AutoCloseable}.
	 *
	 * @see #stop()
	 */
	@Override
	public void close() {
		stop();
	}

	/** @return {@code true} whether this watcher is currently active or in a shutdown process. */
	public synchronized boolean isRunning() {
		return triggerTask != null;
	}

	/**
	 * Class created to hold the task that will be executed in case of the {@link WatchService} detects a change on the file it watches.
	 * 
	 * @author Magno Nascimento
	 */
	private class WatchServiceTriggerTask extends Thread {

		/** A flag used to indicate when the thread should be turned off. */
		private volatile boolean turnOff = false;

		/** Tells the {@link WatchService} to stop watching for events and stops the thread. */
		public void turnOff() {
			turnOff = true;
			interrupt(); //interrupt the thread if it's sleeping so it will be able to shutdown
		}

		@Override
		public void run() {

			WatchKey watchKey = null;

			while(!turnOff) {

				try {
					watchKey = watcher.poll(1, TimeUnit.DAYS);
				} catch(InterruptedException ex) {
					//if we get interrupted here for some reason, the watchKey will probably be null at this point, so all we have to do is start it again.
				}

				if(referenceQueue.poll() != null) { //if the reference for the reloading controller couldn't be found, we must break this looping to finish the execution of this watch service.
					turnOff = true;
				}

				if(turnOff) {
					break; //if the reloading trigger is asked to be turned off we must stop its execution before checking for events on the watch service.
				}

				if(watchKey != null) {

					for(final WatchEvent<?> event : watchKey.pollEvents()) {

						if(event.kind().equals(StandardWatchEventKinds.ENTRY_MODIFY)) { //this statement makes sure that an OVERFLOW event won't be the cause of the check for reloading of the ReloadingController

							@SuppressWarnings("unchecked")
							final Path filename = ((WatchEvent<Path>)event).context();
							if(filename.equals(configurationFilePath.getFileName())) {
								controller.get().checkForReloading(controllerParam);
							}

						}

					}

					final boolean valid = watchKey.reset();

					if(!valid) {
						turnOff = true;
					}
				}

			}

			triggerTask = null;
		}

	}

}
