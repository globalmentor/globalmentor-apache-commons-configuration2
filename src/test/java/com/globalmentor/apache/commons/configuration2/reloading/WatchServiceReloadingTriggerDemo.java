/*
 * Copyright © 2017 GlobalMentor, Inc. <http://www.globalmentor.com/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.globalmentor.apache.commons.configuration2.reloading;

import java.io.*;
import java.nio.file.*;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.ReloadingFileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.reloading.ReloadingController;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.slf4j.*;

/**
 * Demo program created to test the {@link WatchServiceReloadingTrigger} class.
 * 
 * @author Magno Nascimento
 */
public class WatchServiceReloadingTriggerDemo {

	/**
	 * The main method used to run the logic of the demo program.
	 * 
	 * @param args The main arguments of the program, which will not be used for this demo.
	 * @throws IOException if an I/O error occurs.
	 * @throws InterruptedException if the execution of the watch service is interrupted.
	 */
	public static void main(String[] args) throws IOException {

		final Logger logger = LoggerFactory.getLogger(WatchServiceReloadingTriggerDemo.class);

		final Path tempFile = Files.createTempFile("WatchServiceDemoFile", ".properties"); //This temporary file is created based upon the system property "java.io.tmpdir"

		tempFile.toFile().deleteOnExit();

		final ReloadingFileBasedConfigurationBuilder<PropertiesConfiguration> configurationBuilder = new ReloadingFileBasedConfigurationBuilder<PropertiesConfiguration>(
				PropertiesConfiguration.class).configure(new Parameters().properties().setFile(tempFile.toFile()));

		final ReloadingController reloadingController = Mockito.spy(configurationBuilder.getReloadingController());

		try (final WatchServiceReloadingTrigger reloadingTrigger = new WatchServiceReloadingTrigger(reloadingController, null, tempFile)) {

			reloadingTrigger.start();

			if(reloadingTrigger.isRunning()) {
				logger.info("the reloading trigger was successfully started");
			} else {
				logger.error("the reloading trigger could not be started");
			}

			Mockito.verifyZeroInteractions(reloadingController);

			try (final BufferedWriter writer = Files.newBufferedWriter(tempFile)) {
				writer.write("property=after writing this, the reloading trigger should detect the change");
				logger.info("the changes were written to the configuration file");
			} catch(IOException ioException) {
				logger.error("the configuration file couldn't be modified");
			}

			try {
				Thread.sleep(100);
			} catch(final InterruptedException ioException) {
				logger.debug("we woke up while sleeping, but we must keep with the program's execution");
			}

			Mockito.verify(reloadingController, VerificationModeFactory.atLeast(1)).checkForReloading(null);

			logger.info("if we got into this point at the program, the changes were successfully detected and passed to the reloading controller");
		}

		logger.info("the reloading trigger was successfully closed");
	}

}
